import socket # needed for pretty much everything
from urllib.request import urlopen # for external ip
import uuid # for finding mac address
from sys import platform # for finding OS


# Get localhost ip
def local_ip():
	local_ip = socket.gethostbyname(hostname())  # Windows only but avoids proxy errors
	if local_ip == "127.0.1.1":
	    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # When it fails and returns 127.0.1.1
	    s.connect(("8.8.8.8", 80))
	    local_ip = (s.getsockname()[0])
	    s.close()
	return(local_ip)

# Get external ip
def external_ip():
    try:
        externalIP = urlopen('https://ident.me').read().decode('utf8')
    except Exception as e:
        return 'Error: {0}'.format(e)
    else:
        return(externalIP)

# Get mac address
def get_mac():
    mac_num = hex(uuid.getnode()).replace('0x', '').upper()
    mac = ':'.join(mac_num[i: i + 2] for i in range(0, 11, 2))
    return(mac)

# Detect os
def os():
	if platform == "linux":
	    os = "Linux"
	elif platform == "darwin":
	    os = "MAC OS X"
	elif platform == "win32" or "win64":
	    os = "Windows"
	else:
	    os = "Unknown OS"
	return(os)


# Get hostname
def hostname():
	hostname = socket.gethostname()
	return(hostname)

# Get locale
def locale():
	import locale # for finding locale, i have to put this here.
	return(locale.getdefaultlocale())


