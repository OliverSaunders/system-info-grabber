import systeminfograbber as system

# Output os, hostname , mac , local ip and public ip
print("OS         :",system.os())
print("Hostname   :",system.hostname())
print("Locale     :",system.locale())
print("Mac address:",system.get_mac())
print("Local ip   :",system.local_ip())
print("External ip:",system.external_ip())
